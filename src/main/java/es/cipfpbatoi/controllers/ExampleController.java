package es.cipfpbatoi.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ExampleController {

    @GetMapping("/enlaces-view")
    public String tablaEnlacesViewAction(){
        return "tabla_enlaces_view";
    }

    @GetMapping("/form-view")
    public String tablaMultiplicarForm() {
        return "tabla_form_view";
    }

}
