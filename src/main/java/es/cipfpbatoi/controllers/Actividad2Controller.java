package es.cipfpbatoi.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

@Controller
public class Actividad2Controller {

    @ResponseBody
    @GetMapping("/saluda")
    public String saludaAction() {
        return "Hola Mundo";
    }

    @ResponseBody
    @GetMapping("/bingo")
    public String generarBolaAction() {
        Random generadorAleatorios = new Random();
        return "bola: " + generadorAleatorios.nextInt(99) + 1;
    }

    @ResponseBody
    @GetMapping("/fecha/es")
    public String fechaEspanyol() {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy hh:mm:ss");
        return now.format(formatter);
    }

    @ResponseBody
    @GetMapping("/fecha/en")
    public String fechaIngles() {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");
        return now.format(formatter);
    }
}
