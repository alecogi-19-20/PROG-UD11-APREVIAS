package es.cipfpbatoi.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Random;

@Controller
public class Activitat1Controller {

    @ResponseBody
    @GetMapping("/example")
    public String example1() {
        return "<strong>Pepe - Hola Mundo</strong>";
    }

    @ResponseBody
    @GetMapping("/actividad1")
    public String actividad1() {
        Random random = new Random();
        int numero = random.nextInt(101);
        return "<html><body>" +
                "<h1> Bola: " + numero + "</h1>" +
                "</body></html>";

    }

}
