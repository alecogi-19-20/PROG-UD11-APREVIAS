package es.cipfpbatoi.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

@Controller
public class Actividad3Controller {

    @GetMapping("/calculadora")
    public String calculadoraAction() {
        return "calculadora";
    }

    @GetMapping("/suma")
    @ResponseBody
    public String sumaAction(@RequestParam int sumando1,
                             @RequestParam int sumando2) {
        return String.format("%d + %d = %d", sumando1, sumando2, (sumando1 + sumando2));
    }

    @GetMapping("/multiplica")
    @ResponseBody
    public String multiplicaAction(@RequestParam int multiplicador,
                             @RequestParam int multiplicando) {
        return String.format("%d + %d = %d", multiplicando, multiplicador, (multiplicando * multiplicador));
    }

    /**
     * URL: /resta
     * parámetro: operando1
     * parámetro: operando2
     * Contenido a mostrar: operando1-operando2 = resultado1
     *                     operando2-operando1 = resultado2
     *                     Ej. 4-3 = 1
     *                          3-4 = -1
     */

    @GetMapping("/resta")
    @ResponseBody
    public String multiplicaAction(@RequestParam HashMap<String, String> params) {
        String operando1 = params.get("operando1");
        String operando2 = params.get("operando2");
        if (operando1 != null && operando2 != null) {
            try {
                int operando1Numeric = Integer.parseInt(operando1);
                int operando2Numeric = Integer.parseInt(operando2);
                return String.format("%d + %d = %d <br> %d + %d = %d ",
                        operando1Numeric, operando2Numeric,
                        operando2Numeric, operando1Numeric,
                        (operando1Numeric-operando2Numeric),
                        (operando2Numeric-operando1Numeric));
            } catch (NumberFormatException e) {
                return "Los 2 operandos deben ser enteros";
            }
        } else {
            return "Debe indicar los 2 operandos";
        }
    }
}
