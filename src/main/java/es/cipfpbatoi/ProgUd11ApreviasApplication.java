package es.cipfpbatoi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProgUd11ApreviasApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProgUd11ApreviasApplication.class, args);
    }

}
